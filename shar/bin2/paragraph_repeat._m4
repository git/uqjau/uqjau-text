#!/usr/bin/env perl

# --------------------------------------------------------------------
# Synopsis: Filter to repeat paragraphs w/special tokens.  Originally
# made to build 'weighted' "fortune files" for affirmconsole.
# --------------------------------------------------------------------
# Usage: $ourname < STDIN
# --------------------------------------------------------------------
# Rating: tone: obscure tool used: regularly stable: n TBD: y
# --------------------------------------------------------------------
# TBD: minor bug, regex in s/// command for token cleanup is adding extra \n's
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2013 _Tom_Rodman_email
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2013/08/21 11:03:14 $   (GMT)
# $Revision: 1.2 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/text-2012.01.14/shar/bin2/RCS/paragraph_repeat._m4,v $
#      $Log: paragraph_repeat._m4,v $
#      Revision 1.2  2013/08/21 11:03:14  rodmant
#      *** empty log message ***
# --------------------------------------------------------------------

use strict;
use warnings;

BEGIN {$/ = "";}

while(<>)
{
  if(m{(^|\n)\s*\#w(\d+)\|\s*$}ms)
  {
    # As in a line containing only:   #w3|
    # Above token triggers printing of 3 identical paragraphs.

    my $reps = $2;
    my $i;
    
    #s{(^|\n)\s*\#w(\d+)\|\s*\Z}{$1\n}ms;
    s{(^|\n)\s*\#w(\d+)\|\s*$}{$1\n}ms;

    #s{\A\s*\#w(\d+)\|\s*$}{}ms;

    for ($i = 0; $i < $reps; $i++) {
      if ( $_ =~ m{\n\n}ms ) {
        print "$_";
      } 
      else {
        # Use case: exactly one paragraph in the file, and last line in file is not empty.
        print "$_\n";
      }
    }
  }
  else
  {
    print "$_";
  }
}

__END__

Test case:

  $ echo -e 'hi\nthere\n#w3|\n\nho\n\n#w3|\nbye\nnow' | $_c/paragraph_repeat
  hi
  there

  hi
  there

  hi
  there

  ho



  bye
  now


  bye
  now


  bye
  now

another:

  $ echo -e 'hi\nthere\n#w3|\n' | $_c/paragraph_repeat
  hi
  there

  hi
  there

  hi
  there

